const cards = j('.job__card');
const openClass = 'job__card--open';

cards.each(
  (index, card) => {
    const jCard = j(card);
    const button = jCard.find('.job__card_button');
    button.click(
      () => jCard.toggleClass(openClass),
    );
  }
);
