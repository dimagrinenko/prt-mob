j(
  () => j('.owl-carousel').owlCarousel({
    lazyEffect: false,
    stopOnHover : true,
    // navigation:true,
    paginationSpeed : 1000,
    goToFirstSpeed : 2000,
    singleItem : true,
    // autoHeight : true,
    transitionStyle: false,
    // pagination: false,
  })
);
