(function() { "use strict";

window.j = jQuery.noConflict();
'use strict';

var cards = j('.job__card');
var openClass = 'job__card--open';

cards.each(function (index, card) {
  var jCard = j(card);
  var button = jCard.find('.job__card_button');
  button.click(function () {
    return jCard.toggleClass(openClass);
  });
});
'use strict';

var menuToggle = function menuToggle() {
  return j('body').toggleClass('showMenu');
};

j('.hamburger').click(menuToggle);
'use strict';

j(function () {
  return j('.owl-carousel').owlCarousel({
    lazyEffect: false,
    stopOnHover: true,
    // navigation:true,
    paginationSpeed: 1000,
    goToFirstSpeed: 2000,
    singleItem: true,
    // autoHeight : true,
    transitionStyle: false
  });
}); }());